/*

Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript.

Це можливість виконувати одночасно багато задач без очікування виконання завдань у черзі, в якій вони можуть блокувати одна одну.
Такий спосіб підвищчує продуктивність віконання завдань під час роботи із зовнішніми ресурсам, де можливі збої, низька швидкість мережі, великі обїєми інформації та інше.

*/


async function getIp() {
    try {
        return res = await fetch('https://api.ipify.org/?format=json')
        .then((res) => res.json())
        .then((data) => data.ip)
    } catch (error) {
        console.log(error);
    }
}

async function getIpData(ip) {
    try {
        return res = await fetch(`http://ip-api.com/json/${ip}?filds=continent,country,region,city,district`)
        .then((res) => res.json());
    } catch (error) {
        console.error(error);
    }
}

function renderIpDetails(ipDetails) {
    const div = document.createElement('div');
    div.innerHTML = `
    <div>
        <p>Континент: ${ipDetails.continent}</p>
        <p>Країна: ${ipDetails.country}</p>
        <p>Регіон: ${ipDetails.region}</p>
        <p>Місто: ${ipDetails.city}</p>
        <p>Район: ${ipDetails.district}</p>
    </div>
    `;
    document.body.appendChild(div);
}

async function getIpDetails() {
    try {
        const ip = await getIp();
        const ipDetails = await getIpData(ip);
        renderIpDetails(ipDetails);
    } catch (error) {
        console.error(error);
    }
}

const btn = document.getElementById('findIpBtn');
btn.addEventListener('click', getIpDetails);